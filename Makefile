CFLAGS:=-O0 -g3 $(shell pkg-config --cflags libdrm libkms)
CPPFLAGS:=
LDFLAGS:=
LDLIBS:=$(shell pkg-config --libs libdrm libkms) -lm

PROGS:=plane

all: $(PROGS)

plane: plane.o convert.o

clean:
	rm -f $(PROGS) *.o

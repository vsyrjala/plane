/*
 * Copyright (C) 2012 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include "convert.h"

#define clamp(x, min, max) ((x) < (min) ? (min) : (x) > (max) ? (max) : (x))

static void rgb_to_ycbcr(uint8_t *y, uint8_t *cb, uint8_t *cr,
			 uint8_t r, uint8_t g, uint8_t b)
{
	float kr, kb;
	float fr, fg, fb;
	float fy, fcb, fcr;

	if (0) {
		fr = (r - 16) / 235.0f;
		fg = (g - 16) / 235.0f;
		fb = (b - 16) / 235.0f;
	} else {
		fr = r / 255.0f;
		fg = g / 255.0f;
		fb = b / 255.0f;
	}

	switch (0) {
	case 0:
		kr = 0.299f;
		kb = 0.114f;
		break;
	case 1:
		kr = 0.2126f;
		kb = 0.0722f;
		break;
	}

	fy  =  kr * fr + (1.0f - kr - kb) * fg + kb * fb;
	fcb = 0.5f * (fb - fy) / (1.0f - kb);
	fcr = 0.5f * (fr - fy) / (1.0f - kr);

	if (1) {
		fy  = fy * 219.0f + 16.0f;
		fcb = (fcb + 0.5f) * 224.0f + 16.0f;
		fcr = (fcr + 0.5f) * 224.0f + 16.0f;

		*y = clamp((int)fy, 16, 235);
		*cb = clamp((int)fcb, 16, 240);
		*cr = clamp((int)fcr, 16, 240);
	} else {
		fy  = fy * 255.0f;
		fcb = (fcb + 0.5f) * 255.0f;
		fcr = (fcr + 0.5f) * 255.0f;

		*y = clamp((int)fy, 0, 255);
		*cb = clamp((int)fcb, 0, 255);
		*cr = clamp((int)fcr, 0, 255);
	}
}

void convert_rgb_to_yuy2(void *dst, const void *src,
			 int dst_stride, int src_stride,
			 int width, int height)
{
	uint8_t *dst_ptr = dst;
	const uint8_t *src_ptr = src;
	int y;

	for (y = 0; y < height; y++) {
		int x;
		uint8_t cbp, crp;

		for (x = 0; x < width; x += 2) {
			uint8_t y0, cb0, cr0, y1, cb1, cr1;

			rgb_to_ycbcr(&y0, &cb0, &cr0,
				     src_ptr[3*x+0], src_ptr[3*x+1], src_ptr[3*x+2]);
			rgb_to_ycbcr(&y1, &cb1, &cr1,
				     src_ptr[3*x+3], src_ptr[3*x+4], src_ptr[3*x+5]);

			if (x == 0) {
				cbp = cb0;
				crp = cr0;
			}

			dst_ptr[x*2+0] = y0;
			dst_ptr[x*2+1] = (cbp + cb0 * 2 + cb1) >> 2;
			dst_ptr[x*2+2] = y1;
			dst_ptr[x*2+3] = (crp + cr0 * 2 + cr1) >> 2;

			cbp = cb1;
			crp = cr1;
		}

		dst_ptr += dst_stride;
		src_ptr += src_stride;
	}
}

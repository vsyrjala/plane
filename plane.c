/*
 * Copyright (C) 2012 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

#include <time.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <xf86drm.h>
#include <xf86drmMode.h>
#include <drm_fourcc.h>
#include <libkms.h>

#include "convert.h"

//#define dprintf printf
#define dprintf(x...) do {} while (0)

#define min(a,b) ((a) < (b) ? (a) : (b))
#define max(a,b) ((a) > (b) ? (a) : (b))

#define PAGE_SIZE 4096

static int fd;

enum plane_csc_matrix {
	PLANE_CSC_MATRIX_BT601,
	PLANE_CSC_MATRIX_BT709,
};

enum plane_csc_range {
	PLANE_CSC_RANGE_MPEG,
	PLANE_CSC_RANGE_JPEG,
};

struct region {
	int32_t x1;
	int32_t y1;
	int32_t x2;
	int32_t y2;
};

struct buffer {
	struct kms_bo *bo;
	void *virt;
	unsigned int size;
	uint32_t offset[4];
	uint32_t stride[4];
	uint32_t handle[4];
	unsigned int width;
	unsigned int height;
	uint32_t fmt;
	bool interlaced;
	uint32_t fb_id;
};

struct crtc {
	bool dirty;

	uint32_t crtc_id;

	unsigned int dispw;
	unsigned int disph;

	struct buffer buf[2];
	int cur_buf;

	uint32_t original_fb;

	struct {
		uint32_t src_x;
		uint32_t src_y;
		uint32_t fb;
		uint32_t mode;
	} prop;
};

struct plane {
	bool dirty;

	enum plane_csc_matrix csc_matrix;
	enum plane_csc_range csc_range;

	struct buffer buf;

	struct region src; /* 16.16 */
	struct region dst;

	struct crtc *crtc;
	bool enable;
	uint32_t plane_id;

	struct {
		uint32_t src_x;
		uint32_t src_y;
		uint32_t src_w;
		uint32_t src_h;

		uint32_t crtc_x;
		uint32_t crtc_y;
		uint32_t crtc_w;
		uint32_t crtc_h;

		uint32_t fb;
		uint32_t crtc;
	} prop;

	struct {
		float ang;
		float rad_dir;
		float rad;
		int w_dir;
		int w;
		int h_dir;
		int h;
	} state;
};

enum color {
	BLACK,
	WHITE,
	RED,
	GREEN,
	BLUE,
	CYAN,
	MAGENTA,
	YELLOW,
	NUM_COLORS,
};

static const uint32_t colors[2][2][NUM_COLORS] = {
	[PLANE_CSC_MATRIX_BT601] = {
		[PLANE_CSC_RANGE_MPEG] = {
			[BLACK  ] = 0x80108010,
			[WHITE  ] = 0x80eb80eb,
			[RED    ] = 0xf0515051,
			[GREEN  ] = 0x3f683f68,
			[BLUE   ] = 0x5051f051,
			[CYAN   ] = 0x10a9afa9,
			[MAGENTA] = 0xc092c092,
			[YELLOW ] = 0xafa910a9,
		},
		[PLANE_CSC_RANGE_JPEG] = {
			[BLACK  ] = 0x80008000,
			[WHITE  ] = 0x80ff80ff,
			[RED    ] = 0xff4c494c,
			[GREEN  ] = 0x36663666,
			[BLUE   ] = 0x494cff4c,
			[CYAN   ] = 0x00b2b6b2,
			[MAGENTA] = 0xc998c998,
			[YELLOW ] = 0xb6b200b2,
		},
	},
	[PLANE_CSC_MATRIX_BT709] = {
		[PLANE_CSC_RANGE_MPEG] = {
			[BLACK  ] = 0x80108010,
			[WHITE  ] = 0x80eb80eb,
			[RED    ] = 0xf03e613e,
			[GREEN  ] = 0x2e8d2e8d,
			[BLUE   ] = 0x613ef03e,
			[CYAN   ] = 0x10bc9ebc,
			[MAGENTA] = 0xd16dd16d,
			[YELLOW ] = 0x9ebc10bc,
		},
		[PLANE_CSC_RANGE_JPEG] = {
			[BLACK  ] = 0x80008000,
			[WHITE  ] = 0x80ff80ff,
			[RED    ] = 0xff365d36,
			[GREEN  ] = 0x22922292,
			[BLUE   ] = 0x5d36ff36,
			[CYAN   ] = 0x00c8a2c8,
			[MAGENTA] = 0xdd6cdd6c,
			[YELLOW ] = 0xa2c800c8,
		},
	},
};

static const uint32_t color_bars[2][2][8] = {
	[PLANE_CSC_MATRIX_BT601] = {
		[PLANE_CSC_RANGE_MPEG] = {
			0x80b480b4,
			0x8ea22ca2,
			0x2c839c83,
			0x3a704870,
			0xc654b854,
			0xd4416441,
			0x7223d423,
			0x80108010,
		},
		/* FIXME */
		[PLANE_CSC_RANGE_JPEG] = {
			0x80b480b4,
			0x8ea22ca2,
			0x2c839c83,
			0x3a704870,
			0xc654b854,
			0xd4416441,
			0x7223d423,
			0x80108010,
		},
	},
	[PLANE_CSC_MATRIX_BT709] = {
		[PLANE_CSC_RANGE_MPEG] = {
			0x80b480b4,
			0x88a82ca8,
			0x2c919391,
			0x34853f85,
			0xcc3fc13f,
			0xd4336d33,
			0x781cd41c,
			0x80108010,
		},
		/* FIXME */
		[PLANE_CSC_RANGE_JPEG] = {
			0x80b480b4,
			0x88a82ca8,
			0x2c919391,
			0x34853f85,
			0xcc3fc13f,
			0xd4336d33,
			0x781cd41c,
			0x80108010,
		},
	},
};

#define ALIGN(x, a) (((x) + (a) - 1) & ~((a) - 1))

static void plane_enable(struct plane *p, bool enable)
{
	p->enable = enable;
	p->dirty = true;
}

static void plane_commit(struct plane *p)
{
	struct crtc *c = p->crtc;
	int r;

	if (!c->prop.src_x) {
		drmModeObjectPropertiesPtr props;
		uint32_t i;

		props = drmModeObjectGetProperties(fd, c->crtc_id, DRM_MODE_OBJECT_CRTC);
		if (!props)
			return;

		for (i = 0; i < props->count_props; i++) {
			drmModePropertyPtr prop;

			prop = drmModeGetProperty(fd, props->props[i]);
			if (!prop)
				continue;

			if (!strcmp(prop->name, "SRC_X"))
				c->prop.src_x = prop->prop_id;
			else if (!strcmp(prop->name, "SRC_Y"))
				c->prop.src_y = prop->prop_id;
			else if (!strcmp(prop->name, "FB_ID"))
				c->prop.fb = prop->prop_id;
			else if (!strcmp(prop->name, "MODE_ID"))
				c->prop.mode = prop->prop_id;

			drmModeFreeProperty(prop);
		}

		drmModeFreeObjectProperties(props);
	}

	if (!p->prop.src_x) {
		drmModeObjectPropertiesPtr props;
		uint32_t i;

		props = drmModeObjectGetProperties(fd, p->plane_id, DRM_MODE_OBJECT_PLANE);
		if (!props)
			return;

		for (i = 0; i < props->count_props; i++) {
			drmModePropertyPtr prop;

			prop = drmModeGetProperty(fd, props->props[i]);
			if (!prop)
				continue;

			if (!strcmp(prop->name, "SRC_X"))
				p->prop.src_x = prop->prop_id;
			else if (!strcmp(prop->name, "SRC_Y"))
				p->prop.src_y = prop->prop_id;
			else if (!strcmp(prop->name, "SRC_W"))
				p->prop.src_w = prop->prop_id;
			else if (!strcmp(prop->name, "SRC_H"))
				p->prop.src_h = prop->prop_id;
			else if (!strcmp(prop->name, "CRTC_X"))
				p->prop.crtc_x = prop->prop_id;
			else if (!strcmp(prop->name, "CRTC_Y"))
				p->prop.crtc_y = prop->prop_id;
			else if (!strcmp(prop->name, "CRTC_W"))
				p->prop.crtc_w = prop->prop_id;
			else if (!strcmp(prop->name, "CRTC_H"))
				p->prop.crtc_h = prop->prop_id;
			else if (!strcmp(prop->name, "FB_ID"))
				p->prop.fb = prop->prop_id;
			else if (!strcmp(prop->name, "CRTC_ID"))
				p->prop.crtc = prop->prop_id;

			drmModeFreeProperty(prop);
		}

		drmModeFreeObjectProperties(props);
	}

	p->dirty = 1;

	if (p->dirty) {
		drmModePropertySetPtr set;

		set = drmModePropertySetAlloc();
		if (!set)
			return;

		drmModePropertySetAdd(set,
				      c->crtc_id,
				      c->prop.fb,
				      c->cur_buf >= 0 ? c->buf[c->cur_buf].fb_id : c->original_fb);

		drmModePropertySetAdd(set,
				      c->crtc_id,
				      c->prop.src_x,
				      0);

		drmModePropertySetAdd(set,
				      c->crtc_id,
				      c->prop.src_y,
				      0);

#if 0
		drmModePropertySetAdd(set,
				      c->crtc_id,
				      c->prop.mode,
				      0);
#endif

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.fb,
				      p->enable ? p->buf.fb_id : 0);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.crtc,
				      p->crtc->crtc_id);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.src_x,
				      p->src.x1);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.src_y,
				      p->src.y1);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.src_w,
				      p->src.x2 - p->src.x1);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.src_h,
				      p->src.y2 - p->src.y1);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.crtc_x,
				      p->dst.x1);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.crtc_y,
				      p->dst.y1);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.crtc_w,
				      p->dst.x2 - p->dst.x1);

		drmModePropertySetAdd(set,
				      p->plane_id,
				      p->prop.crtc_h,
				      p->dst.y2 - p->dst.y1);

		//r = drmModePropertySetCommit(fd, DRM_MODE_ATOMIC_TEST_ONLY, set);
		r = drmModePropertySetCommit(fd, 0, set);

		drmModePropertySetFree(set);

		if (r) {
			unsigned int src_w = p->src.x2 - p->src.x1;
			unsigned int src_h = p->src.y2 - p->src.y1;
			unsigned int dst_w = p->dst.x2 - p->dst.x1;
			unsigned int dst_h = p->dst.y2 - p->dst.y1;

			printf("setatomic returned %d:%s\n", errno, strerror(errno));

			printf("plane = %u, crtc = %u, fb = %u\n",
			       p->plane_id, p->crtc->crtc_id, p->buf.fb_id);

			printf("src = %u.%06ux%u.%06u+%u.%06u+%u.%06u\n",
			       src_w >> 16, ((src_w & 0xffff) * 15625) >> 10,
			       src_h >> 16, ((src_h & 0xffff) * 15625) >> 10,
			       p->src.x1 >> 16, ((p->src.x1 & 0xffff) * 15625) >> 10,
			       p->src.y1 >> 16, ((p->src.y1 & 0xffff) * 15625) >> 10);

			printf("dst = %ux%u+%d+%d\n",
			       dst_w, dst_h, p->dst.x1, p->dst.y1);

			return;
		}
		usleep(10000);
		p->dirty = false;
	}
}

struct termios saved_termios;

static void init_term(void)
{
	struct termios t;

	tcgetattr(0, &t);

	saved_termios = t;

	t.c_lflag &= ~(ICANON | ECHO | ISIG);
	t.c_cc[VMIN] = 1;
	t.c_cc[VTIME] = 0;

	tcsetattr(0, TCSANOW, &t);
}

static void deinit_term(void)
{
	tcsetattr(0, TCSANOW, &saved_termios);
}

static void fill_packed(struct plane *p, uint32_t color)
{
	struct buffer *buf = &p->buf;
	int x, y;
	uint32_t c;
	uint32_t *ptr;
	int w = buf->width >> 1;
	int h = buf->height;
	int stride = buf->stride[0] >> 2;
	void *packed_ptr = buf->virt;
	bool field;

	packed_ptr += buf->offset[0];

	memset(buf->virt, 0, buf->size);

	ptr = packed_ptr;
	for (y = 0; y < h; y++) {
		field = buf->interlaced && (y & 1);
		c = field ? (p->csc_range == PLANE_CSC_RANGE_MPEG ? 0x80108010 : 0x80008000) : color;

		for (x = 0; x < w; x++)
			ptr[x] = c;

		ptr += stride;
	}

	// paint diagonal line
	if (buf->interlaced) {
		c = p->csc_range == PLANE_CSC_RANGE_MPEG ? 0x80eb80eb : 0x80ff80ff;

		ptr = packed_ptr;
		for (y = 0; y < h/2; y++) {
			for (x = y * w / h; x < w; x += 20)
				ptr[x] = c;
			for (x = y * w / h; x >= 0; x -= 20)
				ptr[x] = c;

			ptr += stride*2;
		}

		ptr = packed_ptr;
		ptr += stride;
		for (y = 0; y < h/2; y++) {
			for (x = y * w / h; x < w; x += 20)
				ptr[x] = c;
			for (x = y * w / h; x >= 0; x -= 20)
				ptr[x] = c;

			ptr += stride*2;
		}
	} else {
		c = p->csc_range == PLANE_CSC_RANGE_MPEG ? 0x80eb80eb : 0x80ff80ff;

		ptr = packed_ptr;
		for (y = 0; y < h; y++) {
			for (x = y * w / h; x < w; x += 20)
				ptr[x] = c;
			for (x = y * w / h; x >= 0; x -= 20)
				ptr[x] = c;

			ptr += stride;
		}
	}

#if 1
	// paint left and right edge
	c = p->csc_range == PLANE_CSC_RANGE_MPEG ? 0x80eb80eb : 0x80ff80ff;

	ptr = packed_ptr;
	for (y = 0; y < h; y++) {
		ptr[0] = ptr[w-1] = c;

		ptr += stride;
	}

	// paint top and bottom edge
	c = p->csc_range == PLANE_CSC_RANGE_MPEG ? 0x80eb80eb : 0x80ff80ff;

	ptr = packed_ptr;
	for (x = 0; x < w; x++)
		ptr[x] = c;
	ptr += (h - 1) * stride;
	for (x = 0; x < w; x++)
		ptr[x] = c;

#endif

#if 0
	ptr = packed_ptr;
	ptr += buf->stride * (p->src.y1 >> 16);
	printf("white starting at off %lx (y=%x x=%d)\n", (long)ptr - (long)buf->virt,
	       p->src.y1 >> 16, p->src.x1 >> 16);
	for (y = (p->src.y1 >> 16); y < ((p->src.y2 + 0xffff) >> 16); y++) {
		uint32_t c = p->csc_range == PLANE_CSC_RANGE_MPEG ? 0x80eb80eb : 0x80ff80ff;
		int i;

		if (y < ((p->src.y1 >> 16) + 8) || y >= (((p->src.y2 + 0xffff) >> 16) - 8)) {
			for (x = (p->src.x1 >> 17); x < ((p->src.x2 + 0xffff) >> 17); x++)
				((uint32_t*)ptr)[x] = c;
		} else {
			x = (p->src.x1 >> 17);
			for (i = 0; i < 8; i++) {
				((uint32_t*)ptr)[x] = c;
				x++;
			}

			x = ((p->src.x2 + 0xffff) >> 17) - 1;
			for (i = 0; i < 8; i++) {
				((uint32_t*)ptr)[x] = c;
				x--;
			}
		}

		ptr += buf->stride[0];
	}
#endif
	printf("filled %d MB w/ %08x\n", buf->size >> 20, color);
}

static void fill_color_bars_packed(struct plane *p)
{
	struct buffer *buf = &p->buf;
	int x, y;
	uint32_t c;
	uint32_t *ptr;
	int w = buf->width >> 1;
	int h = buf->height;
	int stride = buf->stride[0] >> 2;
	void *packed_ptr = buf->virt;
	bool field;

	packed_ptr += buf->offset[0];

	memset(buf->virt, 0, buf->size);

	ptr = packed_ptr;
	for (y = 0; y < h; y++) {
		const uint32_t *c = color_bars[p->csc_matrix][p->csc_range];

		for (x = 0; x < w; x++)
			ptr[x] = c[8*x/w];

		ptr += stride;
	}

}

static void fill_420(struct plane *p, uint32_t color)
{
	struct buffer *buf = &p->buf;
	int x, y;

	int y_w = buf->width;
	int y_h = buf->height;
	int y_stride = buf->stride[0];

	int uv_w = y_w >> 1;
	int uv_h = y_h >> 1;
	int uv_stride = buf->stride[1];

	uint8_t *y_ptr = buf->virt;
	uint8_t *u_ptr = buf->virt;
	uint8_t *v_ptr = buf->virt;

	y_ptr += buf->offset[0];
	u_ptr += buf->offset[1];
	v_ptr += buf->offset[2];

	uint8_t ycolor = color & 0xff;
	uint8_t ucolor = (color >> 8) & 0xff;
	uint8_t vcolor = (color >> 24) & 0xff;

	uint8_t *ptr;
	uint8_t c;
	bool field;

	memset(buf->virt, 0, buf->size);

	// solid fill
	memset(y_ptr, ycolor, y_h * y_stride);
	memset(u_ptr, ucolor, uv_h * uv_stride);
	memset(v_ptr, vcolor, uv_h * uv_stride);

	c = p->csc_range == PLANE_CSC_RANGE_MPEG ? 0xeb : 0xff;

	// draw diaginal
	ptr = y_ptr;
	for (y = 0; y < y_h; y++) {
		for (x = y * y_w / y_h; x < y_w; x += 20)
			ptr[x] = c;
		for (x = y * y_w / y_h; x >= 0; x -= 20)
			ptr[x] = c;
		ptr += y_stride;
	}
	ptr = u_ptr;
	for (y = 0; y < uv_h; y++) {
		for (x = y * uv_w / uv_h; x < uv_w; x += 20)
			ptr[x] = 0x80;
		for (x = y * uv_w / uv_h; x >= 0; x -= 20)
			ptr[x] = 0x80;
		ptr += uv_stride;
	}
	ptr = v_ptr;
	for (y = 0; y < uv_h; y++) {
		for (x = y * uv_w / uv_h; x < uv_w; x += 20)
			ptr[x] = 0x80;
		for (x = y * uv_w / uv_h; x >= 0; x -= 20)
			ptr[x] = 0x80;
		ptr += uv_stride;
	}

	// paint left/right edges
	ptr = y_ptr;
	for (y = 0; y < y_h; y++) {
		ptr[0] = ptr[y_w-1] = c;
		ptr += y_stride;
	}
	ptr = u_ptr;
	for (y = 0; y < uv_h; y++) {
		ptr[0] = ptr[uv_w-1] = 0x80;
		ptr += uv_stride;
	}
	ptr = v_ptr;
	for (y = 0; y < uv_h; y++) {
		ptr[0] = ptr[uv_w-1] = 0x80;
		ptr += uv_stride;
	}

	// paint top and bottom edge
	ptr = y_ptr;
	for (x = 0; x < y_w; x++)
		ptr[x] = c;
	ptr = u_ptr;
	for (x = 0; x < uv_w; x++)
		ptr[x] = 0x80;
	ptr = v_ptr;
	for (x = 0; x < uv_w; x++)
		ptr[x] = 0x80;

	ptr = y_ptr + (y_h - 1) * y_stride;
	for (x = 0; x < y_w; x++)
		ptr[x] = c;
	ptr = u_ptr + (uv_h - 1) * uv_stride;
	for (x = 0; x < uv_w; x++)
		ptr[x] = 0x80;
	ptr = v_ptr + (uv_h - 1) * uv_stride;
	for (x = 0; x < uv_w; x++)
		ptr[x] = 0x80;

	printf("filled %d MB w/ %08x\n", buf->size >> 20, color);
}

static void fill_nv(struct plane *p, uint32_t color)
{
	struct buffer *buf = &p->buf;
	int x, y;

	int y_w = buf->width;
	int y_h = buf->height;
	int y_stride = buf->stride[0];

	int uv_w = y_w >> 1;
	int uv_h = y_h >> 1;
	int uv_stride = buf->stride[1] >> 1;

	uint8_t *y_ptr = buf->virt;
	uint16_t *uv_ptr = buf->virt;

	y_ptr += buf->offset[0];
	uv_ptr += buf->offset[1] >> 1;

	uint8_t ycolor = color & 0xff;
	uint16_t uvcolor = (((color >> 24) & 0xff) << 8) | ((color >> 8) & 0xff);
	uint8_t *ptr;
	uint16_t *ptr2;
	uint8_t c;
	bool field;

	memset(buf->virt, 0, buf->size);

	// solid fill
	memset(y_ptr, ycolor, y_h * y_stride);
	ptr2 = uv_ptr;
	for (y = 0; y < uv_h; y++) {
		for (x = 0; x < uv_w; x++)
			ptr2[x] = uvcolor;
		ptr2 += uv_stride;
	}

	c = p->csc_range == PLANE_CSC_RANGE_MPEG ? 0xeb : 0xff;

	// draw diagonal
	ptr = y_ptr;
	for (y = 0; y < y_h; y++) {
		for (x = y * y_w / y_h; x < y_w; x += 20)
			ptr[x] = c;
		for (x = y * y_w / y_h; x >= 0; x -= 20)
			ptr[x] = c;
			break;
		ptr += y_stride;
	}
	ptr2 = uv_ptr;
	for (y = 0; y < uv_h; y++) {
		for (x = y * uv_w / uv_h; x < uv_w; x += 20)
			ptr2[x] = 0x8080;
		ptr2 += uv_stride;
	}

	// paint left/right edges
	ptr = y_ptr;
	for (y = 0; y < y_h; y++) {
		ptr[0] = ptr[y_w-1] = c;
		ptr += y_stride;
	}
	ptr2 = uv_ptr;
	for (y = 0; y < uv_h; y++) {
		ptr2[0] = ptr2[uv_w-1] = 0x8080;
		ptr2 += uv_stride;
	}

	// paint top and bottom edge
	ptr = y_ptr;
	for (x = 0; x < y_w; x++)
		ptr[x] = c;
	ptr2 = uv_ptr;
	for (x = 0; x < uv_w; x++)
		ptr2[x] = 0x8080;

	ptr = y_ptr + (y_h - 1) * y_stride;
	for (x = 0; x < y_w; x++)
		ptr[x] = c;
	ptr2 = uv_ptr + (uv_h - 1) * uv_stride;
	for (x = 0; x < uv_w; x++)
		ptr2[x] = 0x8080;

	printf("filled %d MB w/ %08x\n", buf->size >> 20, color);
}

static void fill_nv2(struct plane *p, uint32_t color)
{
	struct buffer *buf = &p->buf;
	int x, y;

	int y_w = buf->width;
	int y_h = buf->height;
	int y_stride = buf->stride[0];

	int uv_w = y_w >> 1;
	int uv_h = y_h >> 1;
	int uv_stride = buf->stride[1] >> 1;

	uint8_t *y_ptr = buf->virt;
	uint16_t *uv_ptr = buf->virt;

	y_ptr += buf->offset[0];
	uv_ptr += buf->offset[1] >> 1;

	uint8_t ycolor = color & 0xff;
	uint16_t uvcolor = (((color >> 24) & 0xff) << 8) | ((color >> 8) & 0xff);
	uint8_t *ptr;
	uint16_t *ptr2;
	uint8_t c;
	bool field;

	memset(buf->virt, 0, buf->size);
	// solid fill
	memset(y_ptr, ycolor, y_h * y_stride);
	ptr2 = uv_ptr;
	for (y = 0; y < uv_h; y++) {
		for (x = 0; x < uv_w; x++)
			ptr2[x] = uvcolor;
		ptr2 += uv_stride;
	}

	int colori = RED;

	int y_xstep = 32;
	int uv_xstep = 16;

	int y_ystep = 16;
	int uv_ystep = 8;

	int y_xoff = 0;
	int y_yoff = 0;

	int uv_xoff = 0;
	int uv_yoff = 0;

	// solid fill
	while (y_xoff <= y_w - y_xstep) {
		color = colors[p->csc_matrix][p->csc_range][colori];
		ycolor = color & 0xff;
		uvcolor = ((color >> 16) & 0xff00) | ((color >> 8) & 0xff);
		colori = (colori + 1) % NUM_COLORS;

		if (y_yoff > y_h - y_ystep){
			y_yoff = y_h - y_ystep;
			uv_yoff = uv_h - uv_ystep;
		}

		ptr = y_ptr + y_yoff * y_stride;
		for (y = y_yoff; y < y_h; y++) {
			for (x = y_xoff; x < y_xoff + y_xstep; x++)
				ptr[x] = ycolor;
			ptr += y_stride;
		}
		ptr2 = uv_ptr + uv_yoff * uv_stride;
		for (y = uv_yoff; y < uv_h; y++) {
			for (x = uv_xoff; x < uv_xoff + uv_xstep; x++)
				ptr2[x] = uvcolor;
			ptr2 += uv_stride;
		}

		y_xoff += y_xstep;
		y_yoff += y_ystep;
		uv_xoff += uv_xstep;
		uv_yoff += uv_ystep;
	}

	printf("filled %d MB w/ %08x\n", buf->size >> 20, color);
}

static void draw_crtc_fb(struct buffer *buf, const struct region *r, unsigned int color)
{
	struct region tmp = *r;
	unsigned int *ptr = buf->virt;
	unsigned int x,y;

	// borders
	tmp.x1 -= 10;
	tmp.x2 += 10;
	tmp.y1 -= 10;
	tmp.y2 += 10;

	if (tmp.x1 < 0)
		tmp.x1 = 0;
	if (tmp.x2 > (int)buf->width)
		tmp.x2 = buf->width;
	if (tmp.y1 < 0)
		tmp.y1 = 0;
	if (tmp.y2 > (int)buf->height)
		tmp.y2 = buf->height;

	ptr = buf->virt;
	for (y = 0; y < buf->height; y++) {
		for (x = 0; x < buf->width; x++)
			ptr[x] = color;
		ptr += buf->stride[0] / 4;
	}

	if (tmp.x2 <= tmp.x1 || tmp.y2 <= tmp.y1)
		return;

	ptr = buf->virt;
	ptr += tmp.y1 * buf->stride[0] / 4;
	for (y = tmp.y1; y < tmp.y2; y++) {
		for (x = tmp.x1; x < tmp.x2; x++)
			ptr[x] = color ^ 0x00ffffff;
		ptr += buf->stride[0] / 4;
	}
}

static void *open_ppm(const char *filename, int *w, int *h)
{
	uint8_t *data;
	char header[30] = {};
	ssize_t r;
	size_t len;
	char *ptr = header;

	int fd = open(filename, O_RDONLY);
	if (fd < 0)
		return NULL;

	r = read(fd, header, sizeof header);
	if (r < 0) {
		close(fd);
		return NULL;
	}

	if (sscanf(header, "P6\n%d %d\n255\n", w, h) != 2) {
		close(fd);
		return NULL;
	}
	ptr = strchr(ptr, '\n') + 1;
	ptr = strchr(ptr, '\n') + 1;
	ptr = strchr(ptr, '\n') + 1;
	*ptr = '\0';

	len = 3 * *w * *h;
	lseek(fd, strlen(header), SEEK_SET);

	data = malloc(len);
	if (!data) {
		close(fd);
		return NULL;
	}

	r = read(fd, data, len);
	if (r < 0 || (size_t) r != len) {
		free(data);
		close(fd);
		return NULL;
	}

	close(fd);

	return data;
}

static void load_image_packed(struct buffer *buf, const char *filename)
{
	int w, h;
	void *data;

	memset(buf->virt, 0, buf->size);

	data = open_ppm(filename, &w, &h);
	if (!data)
		return;

	convert_rgb_to_yuy2(buf->virt + buf->offset[0],
			    data, buf->stride[0],
			    w * 3, min(w, buf->width), min(h, buf->height));

	free(data);
}

static bool fmt_is_packed(uint32_t fmt)
{
	switch (fmt) {
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_YVYU:
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_VYUY:
		return true;
	default:
		return false;
	}
}

static bool fmt_is_nv(uint32_t fmt)
{
	switch (fmt) {
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_NV16:
	case DRM_FORMAT_NV61:
		return true;
	default:
		return false;
	}
}

static void fill(struct plane *p, enum color color)
{
	if (fmt_is_packed(p->buf.fmt))
		fill_packed(p, colors[p->csc_matrix][p->csc_range][color]);
	else if (fmt_is_nv(p->buf.fmt))
		fill_nv2(p, colors[p->csc_matrix][p->csc_range][color]);
	else
		fill_420(p, colors[p->csc_matrix][p->csc_range][color]);
}

static void fill_color_bars(struct plane *p)
{
	if (fmt_is_packed(p->buf.fmt))
		fill_color_bars_packed(p);
}

static void load_image(struct plane *p, const char *filename)
{
	if (fmt_is_packed(p->buf.fmt))
		load_image_packed(&p->buf, filename);
}

static void tp_sub(struct timespec *tp, const struct timespec *tp2)
{
	tp->tv_sec -= tp2->tv_sec;
	tp->tv_nsec -= tp2->tv_nsec;
	if (tp->tv_nsec < 0) {
		tp->tv_nsec += 1000000000L;
		tp->tv_sec--;
	}
}

static float adjust_angle(struct plane *p)
{
	const float ang_adj = M_PI / 500.0f;

	p->state.ang += ang_adj;
	if (p->state.ang > 2.0f * M_PI)
		p->state.ang -= 2.0f * M_PI;

	return p->state.ang;
}

static float adjust_radius(struct plane *p)
{
	float rad_max = sqrtf(p->crtc->dispw * p->crtc->dispw + p->crtc->disph * p->crtc->disph);
	float rad_min = -rad_max;
	float rad_adj = rad_max / 500.0f;

	p->state.rad += rad_adj * p->state.rad_dir;
	if (p->state.rad > rad_max && p->state.rad_dir > 0.0f) {
		p->state.rad_dir = -p->state.rad_dir;
	} else if (p->state.rad < rad_min && p->state.rad_dir < 0.0f) {
		p->state.rad_dir = -p->state.rad_dir;
	}

	return p->state.rad;
}

static int adjust_w(struct plane *p)
{
	int w_max = p->crtc->dispw * 4;
	int w_adj = 1;//p->crtc->dispw / 100;

	p->state.w += w_adj * p->state.w_dir;
	if (p->state.w > w_max && p->state.w_dir > 0) {
		p->state.w_dir = -p->state.w_dir;
	} else if (p->state.w < 0 && p->state.w_dir < 0) {
		p->state.w = 0;
		p->state.w_dir = -p->state.w_dir;
	}

	return p->state.w;
}

static int adjust_h(struct plane *p)
{
	int h_max = p->crtc->disph * 4;
	int h_adj = 1;//p->crtc->disph / 100;

	p->state.h += h_adj * p->state.h_dir;
	if (p->state.h > h_max && p->state.h_dir > 0) {
		p->state.h_dir = -p->state.h_dir;
	} else if (p->state.h < 0 && p->state.h_dir < 0) {
		p->state.h = 0;
		p->state.h_dir = -p->state.h_dir;
	}

	return p->state.h;
}

static int alloc_buf(int fd,
		     struct kms_driver *kms,
		     struct buffer *buf,
		     unsigned int fmt,
		     unsigned int width,
		     unsigned int height)
{
	unsigned int attr[] = {
		KMS_BO_TYPE, 0,
		KMS_WIDTH, width,
		KMS_HEIGHT, height,
		KMS_TERMINATE_PROP_LIST,
	};

	switch (fmt) {
	case DRM_FORMAT_XRGB8888:
		attr[1] = KMS_BO_TYPE_SCANOUT_X8R8G8B8;
		break;
	case DRM_FORMAT_YUYV:
		attr[1] = KMS_BO_TYPE_SCANOUT_YUYV;
		break;
	default:
		return 1;
	}

	memset(buf, 0, sizeof *buf);

	buf->fmt = fmt;

	buf->width = width;
	buf->height = height;

	if (kms_bo_create(kms, attr, &buf->bo))
		return 1;

	if (kms_bo_get_prop(buf->bo, KMS_PITCH, &buf->stride[0]))
		return 2;

	buf->size = buf->stride[0] * buf->height;

	if (kms_bo_get_prop(buf->bo, KMS_HANDLE, &buf->handle[0]))
		return 3;

	if (kms_bo_map(buf->bo, &buf->virt))
		return 4;

	if (drmModeAddFB2(fd, width, height, fmt, buf->handle, buf->stride, buf->offset, &buf->fb_id, 0))
		return 5;

	return 0;
}


static void free_buf(int fd, struct buffer *buf)
{
	drmModeRmFB(fd, buf->fb_id);

	kms_bo_unmap(buf->bo);

	kms_bo_destroy(&buf->bo);

	memset(buf, 0, sizeof *buf);
}

int main(int argc, char *argv[])
{
	enum color color = BLACK;
	struct crtc c = {
	};
	struct plane p = {
		.crtc = &c,
		.csc_matrix = PLANE_CSC_MATRIX_BT709,
		.csc_range = PLANE_CSC_RANGE_MPEG,
		.state = {
			.ang = 0.0f,
			.rad_dir = 1.0f,
			.rad = 0.0f,
			.w_dir = 1,
			.w = 0,
			.h_dir = 1,
			.h = 0,
		},
	};
	bool enable = true;
	bool quit = false;
	int r;
	int i;
	struct kms_driver *kms;
	unsigned int handle;
	unsigned int crtc_idx;

	fd = drmOpen("i915", NULL);
	if (fd < 0)
		return 1;

	if (kms_create(fd, &kms))
		return 2;

	drmModeResPtr res = drmModeGetResources(fd);
	if (!res)
		return 3;
	if (res->count_crtcs < 1)
		return 4;

	for (i = 0; i < res->count_crtcs; i++) {
		drmModeCrtcPtr crtc;
		int j;

		crtc = drmModeGetCrtc(fd, res->crtcs[i]);
		if (!crtc)
			continue;

		printf("crtc %u, mode = %ux%u, fb = %u\n", crtc->crtc_id,
		       crtc->mode_valid ? crtc->mode.hdisplay : 0,
		       crtc->mode_valid ? crtc->mode.vdisplay : 0,
		       crtc->buffer_id);

		if (crtc->mode_valid) {
			c.crtc_id = crtc->crtc_id;
			c.dispw = crtc->mode.hdisplay;
			c.disph = crtc->mode.vdisplay;
			c.original_fb = crtc->buffer_id;
		}

		drmModeFreeCrtc(crtc);

		if (c.crtc_id)
			break;
	}
	crtc_idx = i;

	if (!c.crtc_id)
		return 5;

	drmModePlaneResPtr pres = drmModeGetPlaneResources(fd);
	if (!pres)
		return 6;
	if (pres->count_planes < 1 || pres->count_planes > 2)
		return 7;

	for (i = 0; i < pres->count_planes; i++) {
		drmModePlanePtr plane;
		int j;

		plane = drmModeGetPlane(fd, pres->planes[i]);
		if (!plane)
			continue;

		printf("plane %u / crtc %u / possible_crtcs = %x / formats =",
		       plane->plane_id, plane->crtc_id, plane->possible_crtcs);
		for (j = 0; j < plane->count_formats; j++)
			printf(" %04x ", plane->formats[j]);
		printf("\n");

		if (plane->possible_crtcs & (1 << crtc_idx)) {
			p.plane_id = plane->plane_id;
		}

		drmModeFreePlane(plane);
	}

	if (!p.plane_id)
		return 8;

	drmModeFreePlaneResources(pres);

	drmModeFreeResources(res);

	if (alloc_buf(fd, kms, &p.buf, DRM_FORMAT_YUYV, 1024, 576))
		return 9;

	p.src.x1 = 0 << 16;
	p.src.y1 = 0 << 16;
	p.src.x2 = p.buf.width << 16;
	p.src.y2 = p.buf.height << 16;

	p.dst.x1 = 0;
	p.dst.x2 = c.dispw/2;
	p.dst.y1 = 0;
	p.dst.y2 = c.disph/2;

	if (alloc_buf(fd, kms, &c.buf[0], DRM_FORMAT_XRGB8888, c.dispw, c.disph) ||
	    alloc_buf(fd, kms, &c.buf[1], DRM_FORMAT_XRGB8888, c.dispw, c.disph))
		return 10;

	c.cur_buf = 0;

	plane_enable(&p, enable);

	fill(&p, color);

	plane_commit(&p);

	p.dirty = true;
	plane_commit(&p);

	init_term();

	srand(time(NULL));

#define TEST_TIMEOUT_USEC 5000

	bool test_running = false;
	struct timeval timeout = {
		.tv_usec = TEST_TIMEOUT_USEC,
	};
	int frames = 0;
	struct timespec prev;

	while (!quit) {
		char cmd;
		struct timeval *t = test_running ? &timeout : NULL;
		fd_set fds;

		FD_ZERO(&fds);
		FD_SET(STDIN_FILENO, &fds);

		r = select(STDIN_FILENO + 1, &fds, NULL, NULL, t);

		if (r < 0 && errno == EINTR)
			continue;

		if (r == 0) {
			float rad = adjust_radius(&p);
			float ang = adjust_angle(&p);
			int w = adjust_w(&p);
			int h = adjust_h(&p);
			int x = rad * sinf(ang) + c.dispw / 2 - w/2;
			int y = rad * cosf(ang) + c.disph / 2 - h/2;

			p.dst.x1 = x;
			p.dst.y1 = y;
			p.dst.x2 = p.dst.x1 + w;
			p.dst.y2 = p.dst.y1 + h;

			draw_crtc_fb(&c.buf[!c.cur_buf], &p.dst, c.cur_buf ? 0xffffff00 : 0xff00ffff);
			c.cur_buf = !c.cur_buf;

			plane_commit(&p);

			frames++;

			if (frames >= 10000) {
				struct timespec cur;
				clock_gettime(CLOCK_MONOTONIC, &cur);
				struct timespec diff = cur;
				tp_sub(&diff, &prev);
				float secs = (float) diff.tv_sec + diff.tv_nsec / 1000000000.0f;
				printf("%u frames in %f secs, %f fps\n", frames, secs, frames / secs);
				prev = cur;
				frames = 0;
			}

			timeout.tv_sec = 0;
			timeout.tv_usec = TEST_TIMEOUT_USEC;
			continue;
		}

		if (!FD_ISSET(STDIN_FILENO, &fds))
			continue;

		if (read(0, &cmd, 1) < 0)
			break;

		switch (cmd) {
		case 'o':
			enable = !enable;
			plane_enable(&p, enable);
			plane_commit(&p);
			break;
		case 'f':
			color = (color + 1) % NUM_COLORS;
			fill(&p, color);
			break;
		case 'b':
			fill_color_bars(&p);
			break;
		case 'l':
			load_image(&p, "test.ppm");
			break;
		case 'q':
			quit = 1;
			break;
		case 's':
		case 'x':
			p.dst.y1 += (cmd == 's') ? -1 : 1;
			p.dst.y2 += (cmd == 's') ? -1 : 1;
			draw_crtc_fb(&c.buf[!c.cur_buf], &p.dst, c.cur_buf ? 0xffffff00 : 0xff00ffff);
			c.cur_buf = !c.cur_buf;
			plane_commit(&p);
			break;
		case 'S':
		case 'X':
			p.dst.y2 += (cmd == 'S') ? -1 : 1;
			draw_crtc_fb(&c.buf[!c.cur_buf], &p.dst, c.cur_buf ? 0xffffff00 : 0xff00ffff);
			c.cur_buf = !c.cur_buf;
			plane_commit(&p);
			break;
		case 'z':
		case 'c':
			p.dst.x1 += (cmd == 'z') ? -1 : 1;
			p.dst.x2 += (cmd == 'z') ? -1 : 1;
			draw_crtc_fb(&c.buf[!c.cur_buf], &p.dst, c.cur_buf ? 0xffffff00 : 0xff00ffff);
			c.cur_buf = !c.cur_buf;
			plane_commit(&p);
			break;
		case 'Z':
		case 'C':
			p.dst.x2 += (cmd == 'Z') ? -1 : 1;
			draw_crtc_fb(&c.buf[!c.cur_buf], &p.dst, c.cur_buf ? 0xffffff00 : 0xff00ffff);
			c.cur_buf = !c.cur_buf;
			plane_commit(&p);
			break;
		case 't':
			test_running = !test_running;
			if (test_running) {
				clock_gettime(CLOCK_MONOTONIC, &prev);
				frames = 0;
			}
			break;
		}
	}

	c.cur_buf = -1;

	deinit_term();

	plane_enable(&p, false);
	plane_commit(&p);

	free_buf(fd, &c.buf[1]);
	free_buf(fd, &c.buf[0]);
	free_buf(fd, &p.buf);

	kms_destroy(&kms);

	drmClose(fd);

	return 0;
}
